# Blaze MC (v1)

This is a project I normally keep private on my GitLab, both because I started a new version and because it is very old.

For context, the intention was to create a programming language that would compile to command blocks in Minecraft. Both the Rust version and the Minecraft version it's based on are outdated, but I think this project holds value as a handcrafted parser I've written myself.

(Full disclosure, it lexes and parses but I didn't finish the AST and actual compilation steps.)
