use std::fmt;

#[allow(dead_code)]
#[derive(Clone)]
#[derive(Debug)]
pub enum NonTerminal {
    Program,

    MainSequence,
    InstantSequence,
    ClockSequence,
    ManualSequence,

    Sequence,
    SequenceBlock,
    SequenceReturn,
    SequenceReturnValue,
    SequenceStatements,
    SequenceStatement,
    SequenceCall,
    SequenceChain,

    ScoreDeclaration,
    ScoreAssignment,

    SignedExpression,
    Expression,
    AddSub,
    Add,
    Sub,
    Term,
    MulDiv,
    Mul,
    Div,
    Mod,
    Factor
}

impl fmt::Display for NonTerminal {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match *self {
            NonTerminal::Program => write!(f, "Program"),
            NonTerminal::MainSequence => write!(f, "MainSequence"),
            NonTerminal::InstantSequence => write!(f, "InstantSequence"),
            NonTerminal::ClockSequence => write!(f, "ClockSequence"),
            NonTerminal::ManualSequence => write!(f, "ManualSequence"),
            NonTerminal::Sequence => write!(f, "Sequence"),
            NonTerminal::SequenceBlock => write!(f, "SequenceBlock"),
            NonTerminal::SequenceReturn => write!(f, "SequenceReturn"),
            NonTerminal::SequenceReturnValue => write!(f, "SequenceReturnValue"),
            NonTerminal::SequenceStatements => write!(f, "SequenceStatements"),
            NonTerminal::SequenceStatement => write!(f, "SequenceStatement"),
            NonTerminal::SequenceCall => write!(f, "SequenceCall"),
            NonTerminal::SequenceChain => write!(f, "SequenceChain"),
            NonTerminal::ScoreDeclaration => write!(f, "ScoreDeclaration"),
            NonTerminal::ScoreAssignment => write!(f, "ScoreAssignment"),
            NonTerminal::SignedExpression => write!(f, "SignedExpression"),
            NonTerminal::Expression => write!(f, "Expression"),
            NonTerminal::AddSub => write!(f, "AddSub"),
            NonTerminal::Add => write!(f, "Add"),
            NonTerminal::Sub => write!(f, "Sub"),
            NonTerminal::Term => write!(f, "Term"),
            NonTerminal::MulDiv => write!(f, "MulDiv"),
            NonTerminal::Mul => write!(f, "Mul"),
            NonTerminal::Div => write!(f, "Div"),
            NonTerminal::Mod => write!(f, "Mod"),
            NonTerminal::Factor => write!(f, "Factor"),
        }
    }
}
