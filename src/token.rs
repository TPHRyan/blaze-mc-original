use std::cmp;
use std::fmt;

#[allow(non_camel_case_types)]
#[derive(Clone)]
#[derive(Debug)]
pub enum Token {
    IDENTIFIER(String),
    CLOCK,
    INSTANT,
    MANUAL,
    SCORE,
    SEQUENCE,
    RETURN,

    NUMBER(i32),
    COMMAND(String),

    ASSIGN,
    CHAIN,
    DIVIDE,
    LBRACE,
    LPAREN,
    MINUS,
    MODULO,
    MULTIPLY,
    PLUS,
    RBRACE,
    RPAREN,
    SEMICOLON,

    EMPTY,
    END_OF_INPUT
}

impl fmt::Display for Token {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match *self {
            Token::IDENTIFIER(ref s) => write!(f, "IDENTIFIER({})", s),
            Token::CLOCK => write!(f, "CLOCK"),
            Token::INSTANT => write!(f, "INSTANT"),
            Token::MANUAL => write!(f, "MANUAL"),
            Token::SCORE => write!(f, "SCORE"),
            Token::SEQUENCE => write!(f, "SEQUENCE"),
            Token::RETURN => write!(f, "RETURN"),

            Token::NUMBER(n) => write!(f, "NUMBER({})", n),
            Token::COMMAND(ref cmd) => write!(f, "COMMAND({})", cmd),

            Token::ASSIGN => write!(f, "ASSIGN"),
            Token::CHAIN => write!(f, "CHAIN"),
            Token::DIVIDE => write!(f, "DIVIDE"),
            Token::LBRACE => write!(f, "LBRACE"),
            Token::LPAREN => write!(f, "LPAREN"),
            Token::MINUS => write!(f, "MINUS"),
            Token::MODULO => write!(f, "MODULO"),
            Token::MULTIPLY => write!(f, "MULTIPLY"),
            Token::PLUS => write!(f, "PLUS"),
            Token::RBRACE => write!(f, "RBRACE"),
            Token::RPAREN => write!(f, "RPAREN"),
            Token::SEMICOLON => write!(f, "SEMICOLON"),

            Token::EMPTY => write!(f, "EMPTY"),
            Token::END_OF_INPUT => write!(f, "END_OF_INPUT")
        }
    }
}

impl cmp::PartialEq for Token {
    fn eq(&self, other: &Token) -> bool {
        match *self {
            Token::IDENTIFIER(_) => {
                match *other {
                    Token::IDENTIFIER(_) => true,
                    _ => false
                }
            },
            Token::CLOCK => {
                match *other {
                    Token::CLOCK => true,
                    _ => false
                }
            },
            Token::INSTANT => {
                match *other {
                    Token::INSTANT => true,
                    _ => false
                }
            },
            Token::MANUAL => {
                match *other {
                    Token::MANUAL => true,
                    _ => false
                }
            },
            Token::SCORE => {
                match *other {
                    Token::SCORE => true,
                    _ => false
                }
            },
            Token::SEQUENCE => {
                match *other {
                    Token::SEQUENCE => true,
                    _ => false
                }
            },
            Token::RETURN => {
                match *other {
                    Token::RETURN => true,
                    _ => false
                }
            },
            Token::NUMBER(_) => {
                match *other {
                    Token::NUMBER(_) => true,
                    _ => false
                }
            },
            Token::COMMAND(_) => {
                match *other {
                    Token::COMMAND(_) => true,
                    _ => false
                }
            },
            Token::ASSIGN => {
                match *other {
                    Token::ASSIGN => true,
                    _ => false
                }
            },
            Token::CHAIN => {
                match *other {
                    Token::CHAIN => true,
                    _ => false
                }
            },
            Token::DIVIDE => {
                match *other {
                    Token::DIVIDE => true,
                    _ => false
                }
            },
            Token::LBRACE => {
                match *other {
                    Token::LBRACE => true,
                    _ => false
                }
            },
            Token::LPAREN => {
                match *other {
                    Token::LPAREN => true,
                    _ => false
                }
            },
            Token::MINUS => {
                match *other {
                    Token::MINUS => true,
                    _ => false
                }
            },
            Token::MODULO => {
                match *other {
                    Token::MODULO => true,
                    _ => false
                }
            },
            Token::MULTIPLY => {
                match *other {
                    Token::MULTIPLY => true,
                    _ => false
                }
            },
            Token::PLUS => {
                match *other {
                    Token::PLUS => true,
                    _ => false
                }
            },
            Token::RBRACE => {
                match *other {
                    Token::RBRACE => true,
                    _ => false
                }
            },
            Token::RPAREN => {
                match *other {
                    Token::RPAREN => true,
                    _ => false
                }
            },
            Token::SEMICOLON => {
                match *other {
                    Token::SEMICOLON => true,
                    _ => false
                }
            },
            Token::EMPTY => {
                match *other {
                    Token::EMPTY => true,
                    _ => false
                }
            },
            Token::END_OF_INPUT => {
                match *other {
                    Token::END_OF_INPUT => true,
                    _ => false
                }
            },
        }
    }
}