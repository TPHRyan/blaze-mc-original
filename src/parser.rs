use nonterminal::NonTerminal;
use parse_rules::apply_rule;
use parse_table::lookup;
use parse_tree::ParseTree;
use token::Token;

pub struct Parser {
    parser_stack: Vec<Symbol>,
    parsed_stack: Vec<Symbol>,
    rule_stack: Vec<usize>
}

impl Parser {
    pub fn new() -> Parser {
        Parser {
            parser_stack: vec![Symbol::Terminal(Token::END_OF_INPUT), Symbol::NonTerminal(NonTerminal::Program)],
            parsed_stack: Vec::new(),
            rule_stack: Vec::new()
        }
    }

    pub fn add(&mut self, token: Token) -> Result<(), String> {
        // Parse tokens as they come in
        let result: Result<(), String> = loop {
            println!("Stack: {:?}", self.parser_stack);
            println!("Parsed tokens: {:?}", self.parsed_stack);
            let stack_top: Symbol = self.parser_stack.pop().unwrap(); // Can unwrap as the stack is never empty
            match stack_top {
                Symbol::Terminal(stack_token) => {
                    println!("Have token {}.", token);
                    if stack_token == token {
                        println!("Token {} matched!", token);
                        if token != Token::END_OF_INPUT {
                            self.parsed_stack.push(Symbol::Terminal(token.clone()));
                        }
                        break Ok(());
                    } else {
                        break Err(format!("[Parser] Unexpected symbol {}", token));
                    }
                },
                Symbol::NonTerminal(stack_nonterminal) => {
                    match lookup(&stack_nonterminal, &token) {
                        Some(rule_number) => {
                            println!("Have token {} and nonterminal {}.", token, stack_nonterminal);
                            println!("Applying rule {}!", rule_number);
                            if rule_number == 0 {
                                self.parsed_stack.push(Symbol::Terminal(Token::EMPTY));
                            } else {
                                self.parsed_stack.push(Symbol::NonTerminal(stack_nonterminal));
                                self.rule_stack.push(rule_number);
                            }
                            apply_rule(rule_number, &mut self.parser_stack);
                        },
                        None => {
                            break Err(format!("[Parser] Unexpected symbol {}", token))
                        }
                    }
                }
            }
        };
        result
    }

    pub fn get_parse_tree(&self) -> Result<ParseTree, String> {
        ParseTree::from_symbols(&self.parsed_stack, &self.rule_stack)
    }
}

#[derive(Clone)]
#[derive(Debug)]
pub enum Symbol {
    NonTerminal(NonTerminal),
    Terminal(Token)
}
