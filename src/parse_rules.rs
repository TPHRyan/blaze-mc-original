use nonterminal::NonTerminal;
use parser::Symbol;
use token::Token;

lazy_static! {
    static ref PARSE_RULES: [Vec<Symbol>; 40] = [
        // REMEMBER: Rules are RIGHT TO LEFT!
        /*  0*/ vec![],
        /*  1*/ vec![Symbol::Terminal(Token::IDENTIFIER(String::new()))],
        /*  2*/ vec![Symbol::Terminal(Token::NUMBER(0))],
        /*  3*/ vec![Symbol::Terminal(Token::COMMAND(String::new()))],
        /*  4*/ vec![Symbol::NonTerminal(NonTerminal::Program), Symbol::NonTerminal(NonTerminal::MainSequence)],
        /*  5*/ vec![Symbol::NonTerminal(NonTerminal::InstantSequence)],
        /*  6*/ vec![Symbol::NonTerminal(NonTerminal::ClockSequence)],
        /*  7*/ vec![Symbol::NonTerminal(NonTerminal::ManualSequence)],
        /*  8*/ vec![Symbol::NonTerminal(NonTerminal::Sequence)],
        /*  9*/ vec![Symbol::NonTerminal(NonTerminal::Sequence), Symbol::Terminal(Token::INSTANT)],
        /* 10*/ vec![Symbol::NonTerminal(NonTerminal::Sequence), Symbol::Terminal(Token::CLOCK)],
        /* 11*/ vec![Symbol::NonTerminal(NonTerminal::Sequence), Symbol::Terminal(Token::MANUAL)],
        /* 12*/ vec![Symbol::NonTerminal(NonTerminal::SequenceBlock), Symbol::Terminal(Token::IDENTIFIER(String::new())), Symbol::Terminal(Token::SEQUENCE)],
        /* 13*/ vec![Symbol::NonTerminal(NonTerminal::SequenceReturn)],
        /* 14*/ vec![Symbol::Terminal(Token::RBRACE), Symbol::NonTerminal(NonTerminal::SequenceStatements), Symbol::Terminal(Token::LBRACE)],
        /* 15*/ vec![Symbol::NonTerminal(NonTerminal::SequenceStatements), Symbol::Terminal(Token::SEMICOLON), Symbol::NonTerminal(NonTerminal::SequenceStatement)],
        /* 16*/ vec![Symbol::NonTerminal(NonTerminal::SequenceReturnValue), Symbol::Terminal(Token::RETURN)],
        /* 17*/ vec![Symbol::Terminal(Token::SEMICOLON), Symbol::NonTerminal(NonTerminal::SequenceCall)],
        /* 18*/ vec![Symbol::Terminal(Token::SEMICOLON)],
        /* 19*/ vec![Symbol::NonTerminal(NonTerminal::SequenceChain), Symbol::Terminal(Token::IDENTIFIER(String::new()))],
        /* 20*/ vec![Symbol::NonTerminal(NonTerminal::SequenceCall), Symbol::Terminal(Token::CHAIN)],
        /* 21*/ vec![Symbol::NonTerminal(NonTerminal::ScoreDeclaration)],
        /* 22*/ vec![Symbol::NonTerminal(NonTerminal::ScoreAssignment)],
        /* 23*/ vec![Symbol::NonTerminal(NonTerminal::ScoreAssignment), Symbol::Terminal(Token::SCORE)],
        /* 24*/ vec![Symbol::NonTerminal(NonTerminal::SignedExpression), Symbol::Terminal(Token::ASSIGN), Symbol::Terminal(Token::IDENTIFIER(String::new()))],
        /* 25*/ vec![Symbol::NonTerminal(NonTerminal::Expression), Symbol::Terminal(Token::MINUS)],
        /* 26*/ vec![Symbol::NonTerminal(NonTerminal::Expression)],
        /* 27*/ vec![Symbol::NonTerminal(NonTerminal::AddSub), Symbol::NonTerminal(NonTerminal::Term)],
        /* 28*/ vec![Symbol::NonTerminal(NonTerminal::AddSub), Symbol::NonTerminal(NonTerminal::Add)],
        /* 29*/ vec![Symbol::NonTerminal(NonTerminal::AddSub), Symbol::NonTerminal(NonTerminal::Sub)],
        /* 30*/ vec![Symbol::NonTerminal(NonTerminal::Term), Symbol::Terminal(Token::PLUS)],
        /* 31*/ vec![Symbol::NonTerminal(NonTerminal::Term), Symbol::Terminal(Token::MINUS)],
        /* 32*/ vec![Symbol::NonTerminal(NonTerminal::MulDiv), Symbol::NonTerminal(NonTerminal::Factor)],
        /* 33*/ vec![Symbol::NonTerminal(NonTerminal::MulDiv), Symbol::NonTerminal(NonTerminal::Mul)],
        /* 34*/ vec![Symbol::NonTerminal(NonTerminal::MulDiv), Symbol::NonTerminal(NonTerminal::Div)],
        /* 35*/ vec![Symbol::NonTerminal(NonTerminal::MulDiv), Symbol::NonTerminal(NonTerminal::Mod)],
        /* 36*/ vec![Symbol::NonTerminal(NonTerminal::Factor), Symbol::Terminal(Token::MULTIPLY)],
        /* 37*/ vec![Symbol::NonTerminal(NonTerminal::Factor), Symbol::Terminal(Token::DIVIDE)],
        /* 38*/ vec![Symbol::NonTerminal(NonTerminal::Factor), Symbol::Terminal(Token::MODULO)],
        /* 39*/ vec![Symbol::Terminal(Token::RPAREN), Symbol::NonTerminal(NonTerminal::SignedExpression), Symbol::Terminal(Token::LPAREN)],
    ];
}

pub fn apply_rule(rule_number: usize, stack: &mut Vec<Symbol>) {
    stack.append(&mut PARSE_RULES[rule_number].clone());
}

pub fn num_children(rule_number: usize) -> usize {
    PARSE_RULES[rule_number].len()
}
