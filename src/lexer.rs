use std::collections::VecDeque;

use scanner::LexemeUnit;
use token::Token;

pub struct Lexer {
    lexeme_queue: VecDeque<LexemeUnit>
}

impl Lexer {
    pub fn new() -> Lexer {
        return Lexer {
            lexeme_queue: VecDeque::new()
        }
    }

    pub fn add(&mut self, lexeme: LexemeUnit) {
        self.lexeme_queue.push_back(lexeme);
    }
}

impl Iterator for Lexer {
    type Item = Result<Token, String>;

    fn next(&mut self) -> Option<Result<Token, String>> {
        let maybe_lexeme = self.lexeme_queue.pop_front();
        match maybe_lexeme {
            None => { None }
            Some(lexeme) => {
                let lexeme_content = lexeme.content().clone();
                let first_char = lexeme_content.chars().nth(0).unwrap(); // Allowed as all lexemes have 1 character
                match first_char {
                    // Try to match single character tokens
                    // (in alphabetical order for convenience)
                    '=' => {
                        Some(Ok(Token::ASSIGN))
                    },
                    '/' => {
                        Some(Ok(Token::DIVIDE))
                    },
                    '{' => {
                        Some(Ok(Token::LBRACE))
                    },
                    '(' => {
                        Some(Ok(Token::LPAREN))
                    },
                    '%' => {
                        Some(Ok(Token::MODULO))
                    },
                    '*' => {
                        Some(Ok(Token::MULTIPLY))
                    },
                    '+' => {
                        Some(Ok(Token::PLUS))
                    },
                    '}' => {
                        Some(Ok(Token::RBRACE))
                    },
                    ')' => {
                        Some(Ok(Token::RPAREN))
                    },
                    ';' => {
                        Some(Ok(Token::SEMICOLON))
                    },
                    '-' => {
                        let possible_second_char = lexeme_content.chars().nth(1);
                        match possible_second_char {
                            Some(second_char) => {
                                match second_char {
                                    '>' => {
                                        Some(Ok(Token::CHAIN))
                                    },
                                    _ => {
                                        let pos = lexeme.pos();
                                        Some(Err(format!("[Lexer][{}:{}] Unexpected character '{}'!", pos.0, pos.1 + 1, second_char)))
                                    }
                                }
                            },
                            None => {
                                Some(Ok(Token::MINUS))
                            }
                        }
                    },
                    // Check for command
                    '`' => {
                        Some(Ok(Token::COMMAND(lexeme_content)))
                    },
                    _ => {
                        // Check for number
                        if first_char.is_numeric() {
                            Some(Ok(Token::NUMBER(lexeme_content.parse().unwrap())))
                        } else if first_char.is_alphabetic() {
                            Some(Ok(check_reserved_words(Token::IDENTIFIER(lexeme_content))))
                        } else {
                            let pos = lexeme.pos();
                            Some(Err(format!("[Lexer][{}:{}] Unexpected character '{}'!", pos.0, pos.1, first_char)))
                        }
                    }
                }
            }
        }
    }
}

fn check_reserved_words(ident: Token) -> Token {
    match ident {
        Token::IDENTIFIER(s) => {
            return match s.as_ref() {
                "clock" => Token::CLOCK,
                "instant" => Token::INSTANT,
                "manual" => Token::MANUAL,
                "score" => Token::SCORE,
                "sequence" => Token::SEQUENCE,
                "return" => Token::RETURN,
                _ => Token::IDENTIFIER(s)
            }
        },
        _ => {
            return ident;
        }
    }
}