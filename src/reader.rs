use std::fs::File;
use std::io::Read;
use std::io::Result;
use std::path::Path;

pub struct FileReader {
    file_string: String
}

impl FileReader {
    pub fn new<P: AsRef<Path>>(filepath: P) -> Result<FileReader> {
        let mut f = File::open(filepath)?;
        let mut s = String::new();
        f.read_to_string(&mut s)?;
        Ok(
            FileReader {
                file_string: s
            }
        )
    }

    pub fn get_all_content(&self) -> &String {
        &self.file_string
    }
}