use regex::CaptureMatches;
use regex::Match;
use regex::Regex;

const LEXEME_REGEX: &'static str = r"([A-Za-z_][A-Za-z0-9_]*|[0-9]+|`.*?`|->|(\n)|[^\s])";

pub struct Scanner<'r, 't> {
    captures: CaptureMatches<'r, 't>,
    line: usize,
    line_start: usize
}

impl<'r, 't> Scanner<'r, 't> {
    pub fn new(input: &'t str) -> Scanner<'r, 't> {
        lazy_static! {
            static ref LEX_RE: Regex = Regex::new(LEXEME_REGEX).unwrap();
        }
        let captures = LEX_RE.captures_iter(input.clone());
        Scanner {
            captures: captures,
            line: 1,
            line_start: 0
        }
    }
}

impl<'r, 't> Iterator for Scanner<'r, 't> {
    type Item = LexemeUnit;

    fn next(&mut self) -> Option<LexemeUnit> {
        let maybe_captures = self.captures.next();
        match maybe_captures {
            None => { None },
            Some(captures) => {
                let capture: Match;
                let maybe_nl_capture = captures.get(2); // Is the capture a newline?
                match maybe_nl_capture {
                    None => {
                        capture = captures.get(1).unwrap(); // If a capture is returned then 1 is guaranteed
                    }, // Captures already contain a real lexeme
                    Some(nl_capture) => {
                        self.line += 1;
                        self.line_start = nl_capture.start() + 1;
                        let maybe_next_captures = self.captures.next();
                        match maybe_next_captures {
                            None => { return None; },
                            Some(next_captures) => {
                                capture = next_captures.get(1).unwrap(); // If a capture is returned then 1 is guaranteed
                            }
                        }
                    }
                }
                // Offset is found as 0-based (pos - line_start = characters from start)
                // Add 1 because line indexing should start at 1
                let pos = Position(self.line, capture.start() - self.line_start + 1);
                Some(LexemeUnit {
                    pos: pos,
                    content: capture.as_str().to_string()
                })
            }
        }
    }
}

pub struct LexemeUnit {
    pos: Position,
    content: String,
}

impl LexemeUnit {
    pub fn pos(&self) -> &Position {
        &self.pos
    }

    pub fn content(&self) -> &String {
        &self.content
    }
}

pub struct Position(pub usize, pub usize);
