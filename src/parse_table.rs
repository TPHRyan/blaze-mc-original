use nonterminal::NonTerminal;
use token::Token;

pub fn lookup(nonterminal: &NonTerminal, token: &Token) -> Option<usize> {
    match *nonterminal {
        NonTerminal::Program => match *token {
            Token::INSTANT => Some(4),
            Token::CLOCK => Some(4),
            Token::MANUAL => Some(4),
            Token::SEQUENCE => Some(4),
            Token::END_OF_INPUT => Some(0),
            _ => None
        },
        NonTerminal::MainSequence => match *token {
            Token::INSTANT => Some(5),
            Token::CLOCK => Some(6),
            Token::MANUAL => Some(7),
            Token::SEQUENCE => Some(8),
            _ => None
        },
        NonTerminal::InstantSequence => match *token {
            Token::INSTANT => Some(9),
            _ => None
        },
        NonTerminal::ClockSequence => match *token {
            Token::CLOCK => Some(10),
            _ => None
        },
        NonTerminal::ManualSequence => match *token {
            Token::MANUAL => Some(11),
            _ => None
        },
        NonTerminal::Sequence => match *token {
            Token::SEQUENCE => Some(12),
            _ => None
        },
        NonTerminal::SequenceBlock => match *token {
            Token::RETURN => Some(13),
            Token::LBRACE => Some(14),
            _ => None
        },
        NonTerminal::SequenceReturn => match *token {
            Token::RETURN => Some(16),
            _ => None
        },
        NonTerminal::SequenceReturnValue => match *token {
            Token::IDENTIFIER(_) => Some(17),
            Token::SEMICOLON => Some(18),
            _ => None
        },
        NonTerminal::SequenceCall => match *token {
            Token::IDENTIFIER(_) => Some(19),
            _ => None
        },
        NonTerminal::SequenceChain => match *token {
            Token::CHAIN => Some(20),
            Token::SEMICOLON => Some(0),
            _ => None
        },
        NonTerminal::SequenceStatements => match *token {
            Token::RETURN => Some(13),
            Token::COMMAND(_) => Some(15),
            Token::IDENTIFIER(_) => Some(15),
            Token::SCORE => Some(15),
            Token::SEMICOLON => Some(15),
            Token::RBRACE => Some(0),
            _ => None
        },
        NonTerminal::SequenceStatement => match *token {
            Token::COMMAND(_) => Some(3),
            Token::SCORE => Some(21),
            Token::IDENTIFIER(_) => Some(22),
            Token::SEMICOLON => Some(22),
            _ => None
        },
        NonTerminal::ScoreDeclaration => match *token {
            Token::SCORE => Some(23),
            _ => None
        },
        NonTerminal::ScoreAssignment => match *token {
            Token::IDENTIFIER(_) => Some(24),
            Token::SEMICOLON => Some(0),
            _ => None
        },
        NonTerminal::SignedExpression => match *token {
            Token::MINUS => Some(25),
            Token::LPAREN => Some(26),
            Token::NUMBER(_) => Some(26),
            Token::IDENTIFIER(_) => Some(26),
            _ => None
        },
        NonTerminal::Expression => match *token {
            Token::LPAREN => Some(27),
            Token::NUMBER(_) => Some(27),
            Token::IDENTIFIER(_) => Some(27),
            _ => None
        },
        NonTerminal::Term => match *token {
            Token::LPAREN => Some(32),
            Token::NUMBER(_) => Some(32),
            Token::IDENTIFIER(_) => Some(32),
            _ => None
        },
        NonTerminal::Factor => match *token {
            Token::LPAREN => Some(39),
            Token::NUMBER(_) => Some(2),
            Token::IDENTIFIER(_) => Some(1),
            _ => None
        },
        NonTerminal::AddSub => match *token {
            Token::PLUS => Some(28),
            Token::MINUS => Some(29),
            Token::RPAREN => Some(0),
            Token::SEMICOLON => Some(0),
            _ => None
        },
        NonTerminal::Add => match *token {
            Token::PLUS => Some(30),
            _ => None
        },
        NonTerminal::Sub => match *token {
            Token::MINUS => Some(31),
            _ => None
        },
        NonTerminal::MulDiv => match *token {
            Token::MULTIPLY => Some(33),
            Token::DIVIDE => Some(34),
            Token::MODULO => Some(35),
            Token::PLUS => Some(0),
            Token::MINUS => Some(0),
            Token::RPAREN => Some(0),
            Token::SEMICOLON => Some(0),
            _ => None
        },
        NonTerminal::Mul => match *token {
            Token::MULTIPLY => Some(36),
            _ => None
        },
        NonTerminal::Div => match *token {
            Token::DIVIDE => Some(37),
            _ => None
        },
        NonTerminal::Mod => match *token {
            Token::MODULO => Some(38),
            _ => None
        }
    }
}
