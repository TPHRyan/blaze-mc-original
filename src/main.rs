#[macro_use] extern crate lazy_static;
extern crate regex;

mod ast_generator;
mod ast_nodes;
mod lexer;
mod nonterminal;
mod parser;
mod parse_rules;
mod parse_table;
mod parse_tree;
mod reader;
mod scanner;
mod token;

use std::error::Error;
use reader::FileReader;

fn main() {
    match FileReader::new("../input.blz") {
        Ok(reader) => {
            match compile(reader) {
                Ok(s) => {
                    println!("{}", s);
                },
                Err(e) => {
                    println!("{}", e);
                }
            }
        },
        Err(e) => {
            println!("[Reader] Failed to read file: {}", e.description());
        }
    };
}

fn compile(reader: FileReader) -> Result<&'static str, String> {
    let content = reader.get_all_content();
    let scanner = scanner::Scanner::new(content);
    let mut lexer = lexer::Lexer::new();
    for lexeme in scanner {
        lexer.add(lexeme);
    }
    let mut parser = parser::Parser::new();
    for error_token in lexer {
        let token = error_token?;
        parser.add(token)?;
    }
    parser.add(token::Token::END_OF_INPUT)?;
    let parse_tree = parser.get_parse_tree()?;
    println!("Generated parse tree: \n{}", parse_tree);
    let ast = ast_generator::ASTGenerator::generate_from_parse_tree(parse_tree);
    println!("Generated abstact syntax tree: \n{}", ast);
    Ok("Compiled successfully.")
}
