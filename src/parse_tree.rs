use std::boxed::Box;
use std::fmt;
use std::slice::Iter;
use nonterminal::NonTerminal;
use parse_rules::num_children;
use parser::Symbol;
use token::Token;

#[derive(Debug)]
pub struct ParseTree {
    pub root_node: ParseTreeNode
}

impl ParseTree {
    pub fn from_symbols(symbols: &Vec<Symbol>, rules: &Vec<usize>) -> Result<ParseTree, String> {
        let mut symbol_iter: Iter<Symbol> = symbols.iter();
        let mut rule_iter: Iter<usize> = rules.iter();
        let root_node_result: Result<ParseTreeNode, String>;
        root_node_result = ParseTree::build_tree(&mut symbol_iter, &mut rule_iter);
        match root_node_result {
            Ok(root_node) => Ok(
                ParseTree {
                    root_node
                }
            ),
            Err(err) => Err(err)
        }
    }

    fn build_tree(mut symbol_iter: &mut Iter<Symbol>, mut rule_iter: &mut Iter<usize>) -> Result<ParseTreeNode, String> {
        let next_symbol = symbol_iter.next();
        match next_symbol {
            None => {
                return Ok(ParseTreeNode::Terminal(Token::END_OF_INPUT));
//                return Err(format!("Invalid symbol vector: Ran out of symbols and tree is not yet fully constructed!"));
            },
            Some(&Symbol::Terminal(ref token)) => {
                println!("Tree: {:?}", token);
                return Ok(ParseTreeNode::Terminal(token.clone()));
            },
            Some(&Symbol::NonTerminal(ref nonterminal)) => {
                println!("Tree: {:?}", nonterminal);
                let processed_rule = rule_iter.next();
                match processed_rule {
                    None => {
                        return Err(format!("Invalid rule vector: Ran out of rules and tree is not yet fully constructed!"));
                    },
                    Some(rule_number) => {
                        let mut new_node = NonTerminalNode::from_nonterminal(nonterminal.clone(), rule_number.clone());
                        for _ in 0..new_node.get_n_children() {
                            new_node.add_child(ParseTree::build_tree(&mut symbol_iter, &mut rule_iter)?);
                        }
                        return Ok(ParseTreeNode::NonTerminal(Box::new(new_node)));
                    }
                }
            }
        }
    }
}

impl fmt::Display for ParseTree {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "<parse-tree>\n{}\n</parse-tree>", self.root_node)
    }
}

#[derive(Debug)]
pub struct NonTerminalNode {
    pub symbol: NonTerminal,
    n_children: usize,
    children: Vec<ParseTreeNode>
}

impl fmt::Display for NonTerminalNode {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "<nonterminal>\n<symbol>{}</symbol>\n{}</nonterminal>", self.symbol, self.get_children_xml())
    }
}

impl NonTerminalNode {
    pub fn from_nonterminal(nonterminal: NonTerminal, rule_number: usize) -> NonTerminalNode {
        NonTerminalNode {
            symbol: nonterminal,
            n_children: num_children(rule_number),
            children: Vec::new()
        }
    }

    pub fn add_child(&mut self, child: ParseTreeNode) {
        self.children.push(child);
    }

    pub fn get_children_xml(&self) -> String {
        let mut children_xml: String = String::new();
        for child in &self.children {
            children_xml.push_str(&format!("<child>\n{}\n</child>\n", child));
        }
        children_xml
    }

    pub fn get_n_children(&self) -> usize {
        self.n_children
    }
}

#[derive(Debug)]
pub enum ParseTreeNode {
    NonTerminal(Box<NonTerminalNode>),
    Terminal(Token)
}

impl fmt::Display for ParseTreeNode {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match *self {
            ParseTreeNode::Terminal(ref token) => {
                write!(f, "<terminal>{}</terminal>", token)
            },
            ParseTreeNode::NonTerminal(ref nt_box) => {
                write!(f, "{}", *nt_box)
            }
        }
    }
}
