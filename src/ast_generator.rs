use std::fmt;
use ast_nodes::ProgramNode;
use parse_tree::ParseTree;

pub struct ASTGenerator {}

impl ASTGenerator {
    pub fn generate_from_parse_tree(_parse_tree: ParseTree) -> AbstractSyntaxTree {
        AbstractSyntaxTree {
            root_node: ProgramNode {
            }
        }
    }
}

pub struct AbstractSyntaxTree {
    pub root_node: ProgramNode
}

impl fmt::Display for AbstractSyntaxTree {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "")
    }
}
