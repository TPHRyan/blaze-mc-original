" Vim syntax file
" Language: BLAZE for Minecraft
" Maintainer: Ryan Paroz
" Latest Revision: 27 August 2017

if exists("b:current_syntax")
    finish
endif

syn match blazeComment "//.*$" contains=@Spell
syn match blazeSequenceIdentifier "[A-Za-z_][A-Za-z0-9_]*" contained

syn region blazeSequenceBlock start="{" end="}" fold transparent
syn region blazeCommand start="`" end="`"

syn keyword blazeAssignmentKeywords score skipwhite
syn keyword blazeSequenceKeywords sequence nextgroup=blazeSequenceIdentifier skipwhite
syn keyword blazeSequenceTypeKeywords clock instant manual

let b:current_syntax = "blaze"

hi def link blazeComment Comment
hi def link blazeSequenceIdentifier Function
hi def link blazeCommand Character
hi def link blazeAssignmentKeywords Type
hi def link blazeSequenceKeywords Type
hi def link blazeSequenceTypeKeywords Typedef
